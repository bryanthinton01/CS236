#pragma once
#include <cctype>
#include <fstream>
#include <vector>
#include <string>
#include "Token.h"

using namespace std;

class Scanner
{
public:
	Scanner();
	void scanTok(vector<Token>&, ifstream&, int&, int&, Token);
	string scanString(vector<Token>&, ifstream&, int&, int&, Token, int&);
	string scanComment(vector<Token>&, ifstream&, int&, int&, Token);
	string scanMLComment(vector<Token>&, ifstream&, int&, int&, Token, string& tempString);
	string scanStringNQ(vector<Token>&, ifstream&, int&, int&, Token);
	void colonCase(vector<Token>&, ifstream&, int&, int&, Token);
	void commentCase(vector<Token>&, ifstream&, int&, int&, Token);
	void defaultCase(vector<Token>&, ifstream&, int&, int&, Token);
	void qCase(vector<Token>&, ifstream&, int&, int&, Token);
	void fCase(vector<Token>&, ifstream&, int&, int&, Token);
	void sCase(vector<Token>&, ifstream&, int&, int&, Token);
	void rCase(vector<Token>&, ifstream&, int&, int&, Token);
	bool stringExt(int&, int&, string&, int&, ifstream&);
	void lineInc(int&, int&);

};