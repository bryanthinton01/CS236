#pragma once
#include "Tuple.h"
#include <set>
#include "Scheme.h"

using namespace std;

class relation
{
public:
	relation select(int, string);
	relation project(vector<int>&);
	relation rename(scheme&);
	void setName(string);
	void setScheme(scheme);
	void setTuple(set<tuples>&);
	string getName();
	scheme getScheme();
	set<tuples> getTuple();
	void addTuples(tuples& in)
	{
		tupleSet.insert(in);
	}
	relation selectID(int, int);
	relation join(scheme&, set<tuples>&);
	scheme combineSchemes(scheme&, vector<int>&);
	bool isJoinable(scheme&, tuples&, tuples&);
	int getNumTuples();

private:
	set<tuples> tupleSet;
	scheme schemeName;
	string name;
};

void relation::setName(string in)
{
	name = in;
}

void relation::setScheme(scheme in)
{
	schemeName = in;
}

void relation::setTuple(set<tuples>& in)
{
	tupleSet = in;
}

string relation::getName()
{
	return name;
}

scheme relation::getScheme()
{
	return schemeName;
}

set<tuples> relation::getTuple()
{
	return tupleSet;
}

relation relation::select(int a, string b)
{
	relation temp;
	set<tuples> tempTuple;
	temp.setName(name);
	temp.setScheme(schemeName);
	for (auto i = tupleSet.begin(); i != tupleSet.end(); i++)
	{
		if ((*i)[a] == b)
		{
			tempTuple.insert(*i);
		}
	}
	temp.setTuple(tempTuple);
	return temp;
}

relation relation::selectID(int orig, int b)
{
	relation temp;
	set<tuples> tempTuple;
	temp.setName(name);
	temp.setScheme(schemeName);
	for (auto i = tupleSet.begin(); i != tupleSet.end(); i++)
	{
		if ((*i)[orig] == (*i)[b])
		{
			tempTuple.insert(*i);
		}
	}
	temp.setTuple(tempTuple);
	return temp;
}

relation relation::project(vector<int>& a)
{
	relation temp;
	temp.setName(name);
	scheme tempScheme;
	set<tuples> tempTuple;
	tuples tempSingleTuple;
	for (unsigned int i = 0; i < a.size(); i++)
	{
		tempScheme.push_back(schemeName[a[i]]);
	}
	temp.setScheme(tempScheme);
	for (auto i = tupleSet.begin(); i != tupleSet.end(); i++)
	{
		tempSingleTuple.clear();
		for (unsigned int y = 0; y < a.size(); y++)
		{
			int q = a[y];
			tempSingleTuple.push_back((*i)[q]);
		}
		tempTuple.insert(tempSingleTuple);
	}
	temp.setTuple(tempTuple);
	return temp;
}

relation relation::rename(scheme& a)
{
	relation temp;
	temp.setScheme(a);
	temp.setTuple(tupleSet);
	temp.setName(name);
	return temp;
}

scheme relation::combineSchemes(scheme& s2, vector<int>& vec)
{
	scheme tempScheme = schemeName;
	for (unsigned int i = 0; i < s2.size(); i++)
	{
		bool inScheme = false;
		for (unsigned int y = 0; y < tempScheme.size(); y++)
		{
			if (s2[i] == tempScheme[y])
				inScheme = true;
		}
		if (inScheme == false)
		{
			tempScheme.push_back(s2[i]);
			vec.push_back(i);
		}
	}
	return tempScheme;
}

bool relation::isJoinable(scheme& s2, tuples& t1, tuples& t2)
{
	for (unsigned int i = 0; i < schemeName.size(); i++)
	{
		for (unsigned int y = 0; y < s2.size(); y++)
		{
			if (schemeName[i] == s2[y] && t1[i] != t2[y])
				return false;
		}
	}
	return true;
}

relation relation::join(scheme& s2, set<tuples>& t2)
{
	relation newRelation;
	vector<int> addedSchemeValues;
	newRelation.setScheme(combineSchemes(s2, addedSchemeValues));
	for (auto i = tupleSet.begin(); i != tupleSet.end(); i++)
	{
		
		for (auto y = t2.begin(); y != t2.end(); y++)
		{
			tuples firstTuple = *i;
			tuples* secondTuple = (tuples*)&(*y);
			if (isJoinable(s2, firstTuple, *secondTuple))
			{
				for (unsigned int z = 0; z < addedSchemeValues.size(); z++)
				{
					firstTuple.push_back((*secondTuple)[addedSchemeValues[z]]);
				}
				newRelation.addTuples(firstTuple);
			}
		}
	}
	return newRelation;
}

int relation::getNumTuples()
{
	int numTuples = 0;
	for (auto it = tupleSet.begin(); it != tupleSet.end(); it++)
		numTuples++;
	return numTuples;
}