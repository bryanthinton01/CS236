#pragma once
#include <vector>
#include "Predicate.h"
#include "Rule.h"

using namespace std;

class datalogPrograms
{
public:
	datalogPrograms();

	void addFacts(predicates in)
	{
		facts.push_back(in);
	}
	void addSchemes(predicates in)
	{
		schemes.push_back(in);
	}
	void addRules(rules in)
	{
		rules_vec.push_back(in);
	}
	void addQueries(predicates in)
	{
		queries.push_back(in);
	}
	vector<predicates> getFacts()
	{
		return facts;
	}
	vector<predicates> getSchemes()
	{
		return schemes;
	}
	vector<predicates> getQueries()
	{
		return queries;
	}
	vector<rules> getRules()
	{
		return rules_vec;
	}

private:
	vector<predicates> schemes;
	vector<predicates> facts;
	vector<rules> rules_vec;
	vector<predicates> queries;
};

datalogPrograms::datalogPrograms()
{

}