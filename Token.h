#pragma once
#include <iostream>
#include <map>
#include <string>

using namespace std;

class Token
{
public:
	enum TokenType
	{
		COMMA, PERIOD, Q_MARK, LEFT_PAREN, RIGHT_PAREN, COLON, COLON_DASH, MULTIPLY, ADD, SCHEMES, FACTS, RULES, QUERIES, ID, STRING, COMMENT, WHITESPACE, UNDEFINED, ENDOFFILE
	};


	void setType(TokenType in)
	{
		Type = in;
	}
	void setValue(string in)
	{
		Value = in;
	}
	void setLineNum(int in)
	{
		LineNum = in;
	}

	string getType()
	{
		enums[0] = "COMMA"; enums[1] = "PERIOD";  enums[2] = "Q_MARK"; enums[3] = "LEFT_PAREN"; enums[4] = "RIGHT_PAREN"; enums[5] = "COLON"; enums[6] = "COLON_DASH"; enums[7] = "MULTIPLY"; enums[8] = "ADD"; enums[9] = "SCHEMES";
		enums[10] = "FACTS"; enums[11] = "RULES"; enums[12] = "QUERIES"; enums[13] = "ID"; enums[14] = "STRING"; enums[15] = "COMMENT"; enums[16] = "WHITESPACE";  enums[17] = "UNDEFINED"; enums[18] = "EOF";
		string returnValue = enums[Type];
		return returnValue;
	}

	string getValue()
	{
		return Value;
	}
	int getLineNum()
	{
		return LineNum;
	}

protected:
	TokenType Type;
	string Value;
	int LineNum;
	map<int, string> enums;


};