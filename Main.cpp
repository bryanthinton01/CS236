#include <cctype>
#include <fstream>
#include <vector>
#include <string>
#include "Parser.h"
#include "Interpreter.h"

using namespace std;

int main(int argc, char* argv[])
{
	int c = 0;
	int lineNum = 1;
	Token tok;
	string value;
	vector<Token> toks;
	ifstream in;
	in.open(argv[1]);
	Scanner scan;
	Parser parse;

	while (c != -1)
	{
		c = in.get();
		scan.scanTok(toks, in, lineNum, c, tok);
	}
	/*int totalTokens = toks.size();
	for (unsigned int i = 0; i < toks.size(); i++)
	{
		cout << "(" << toks[i].getType() << "," << "\"" << toks[i].getValue() << "\"" << "," << toks[i].getLineNum() << ")" << endl;
	}
	cout << "Total Tokens = " << totalTokens;*/
	datalogPrograms program = parse.Parse(toks);
	interpreter queriedProgram;
	queriedProgram.addSchemes(program);
	queriedProgram.addTuples(program);
	//int numPasses = 0;
	queriedProgram.createGraph(program);
	queriedProgram.outDependencyGraph();
	queriedProgram.toFixedPoint(program);//.fixedPoint(program);//, numPasses);
	//cout << "Schemes populated after " << numPasses << " passes through the Rules.\n";
	queriedProgram.doQuery(program);
	queriedProgram.printQuery(program);
	
	return 0;
}