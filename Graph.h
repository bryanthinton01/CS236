#pragma once
#include <map>
#include "Node.h"
#include <vector>

using namespace std;

class graph
{
public:
	void dfs(node&);
	void dfsForest();
	void addEdge(int, int);
	void dfsForestFromReverse(graph&);
	map<int, node> getMap()
	{
		return graphMap;
	}
	void insertIntoGraph(int fromWhere)
	{
		node blankNode;
		/*bool exists = false;
		for (auto i = graphMap.begin(); i != graphMap.end(); i++)
		{
			if (i->first == fromWhere)
				exists = true;
		}
		if (!exists)*/
			graphMap.insert(pair<int, node>(fromWhere, blankNode));
		//else
			//return;
		return;
	}
	//bool isCyclic();
	void dfsFromReverse(node&, set<int>&);//, node&, map<int, node>&);
	set<int> getSCC(int x)
	{
		return SCC[x];
	}
	int getSCCSize()
	{
		return SCC.size();
	}
	vector<set<int>> getSCCVec()
	{
		return SCC;
	}

private:
	map<int, node> graphMap;
	int postOrderCount = 0;
	vector<set<int>> SCC;
	//vector<int> cyclic;
};

void graph::dfs(node& x)
{
	x.markVisited(true);
	set<int> nodeSet = x.getNodeSet();
	for (auto y = nodeSet.begin(); y != nodeSet.end(); y++)
	{
		int q = *y;
		if (graphMap[q].alreadyVisited() == false)
			dfs(graphMap[q]);
	}
	x.setPostOrderNumber(postOrderCount);
	postOrderCount++;
}
void graph::dfsFromReverse(node& x, set<int>& tempSet)//, node& temp, map<int, node>& reverseMap)
{
	if (x.alreadyVisited() == true)
		return;
	x.markVisited(true);
	set<int> nodeSet = x.getNodeSet();
	for (auto y = nodeSet.begin(); y != nodeSet.end(); y++)
	{
		int q = *y;
		if (graphMap[q].alreadyVisited() == false)
		{
			tempSet.insert(q);
			dfsFromReverse(graphMap[q], tempSet);//, reverseMap[q], reverseMap);
		}
	}
	//SCC.push_back(tempSet);

	//x.setPostOrderNumber(temp.getPostOrderNumber());
}
void graph::dfsForest()
{
	for (auto i = graphMap.begin(); i != graphMap.end(); i++)
	{
		int x = i->first;
		if (graphMap[x].alreadyVisited() == false)
			dfs(graphMap[x]);
	}
}
void graph::addEdge(int fromWhere, int toWhere)
{
	node blankNode;
	graphMap.insert(pair<int, node>(fromWhere, blankNode));
	for (auto it = graphMap.begin(); it != graphMap.end(); it++)
	{
		if (it->first == fromWhere)
		{
			it->second.addToNodeSet(toWhere);
			break;
		}
	}
}
void graph::dfsForestFromReverse(graph& reverse)
{
	map<int, node> temp = reverse.getMap();
	for (int i = temp.size() - 1; i >= 0; i--)
	{
		set<int> tempSet;
		for (auto j = temp.begin(); j != temp.end(); j++)
		{
			int x = j->first;
			if (graphMap[x].alreadyVisited() == false && j->second.getPostOrderNumber() == (int)i)
			{
				//graphMap[x].markVisited(true);
				//set<int> tempSet;
				tempSet.insert(x);
				dfsFromReverse(graphMap[x], tempSet);//, j->second, temp);
				//graphMap[x].setPostOrderNumber(j->second.getPostOrderNumber());
				//postOrderCount++;
				break;
			}
		}
		SCC.push_back(tempSet);
	}
	if (SCC.size() == 0)
	{
		set<int> tempSet;
		tempSet.insert(0);
		SCC.push_back(tempSet);
	}
}
/*bool graph::isCyclic()
{
	//for each edge x->y (for each node and for each adjacent node)
	for (auto i = graphMap.begin(); i != graphMap.end(); i++)
	{
		set<int> tempSet = i->second.getNodeSet();
		for (auto j = tempSet.begin(); j != tempSet.end(); j++)
		{
			for (auto k = graphMap.begin(); k != graphMap.end(); k++)
			{
				if (i->first == *j)
				{
					if (i->second.getPostOrderNumber() <= k->second.getPostOrderNumber())
					{
						cyclic.push_back(i->first);
						cyclic.push_back(k->first);
						return true;
						break;
					}
				}
			}
		}
	}
	return false;
}*/