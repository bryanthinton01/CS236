#pragma once
#include "Relation.h"
#include <map>

using namespace std;

class database
{
public:
	void addRelation(string name, scheme schemeIn)
	{
		relation temp;
		set<tuples> tempTup;
		temp.setName(name);
		temp.setScheme(schemeIn);
		temp.setTuple(tempTup);
		relationMap.insert({ name, temp });
	}
	void addTuple(string name, tuples in)
	{
		relationMap[name].addTuples(in);
	}
	relation getRelation(string name)
	{
		return relationMap[name];
	}
	int getSize()
	{
		int size = 0;
		for (auto it = relationMap.begin(); it != relationMap.end(); it++)
		{
			size += it->second.getNumTuples();
		}
		return size;
	}
private:
	map<string, relation> relationMap;
};