#pragma once
#include "Database.h"
#include "DatalogProgram.h"
#include <iostream>
#include "Graph.h"
#include <string>

using namespace std;

class interpreter
{
public:
	interpreter();
	void addSchemes(datalogPrograms&);
	void addTuples(datalogPrograms&);
	void doQuery(datalogPrograms&);
	void printQuery(datalogPrograms&);
	void selectQuery(tuples&, vector<bool>&, scheme&, relation&);
	void printResult(unsigned int& i);
	void evaluateRule(datalogPrograms&, int);
	void fixedPoint(datalogPrograms&, int&, vector<int>);
	void projectAndRenameAfterJoin(datalogPrograms&, relation&, int);
	void doRule(datalogPrograms&, int, vector<relation>&);
	void createGraph(datalogPrograms&);
	void outDependencyGraph();
	void toFixedPoint(datalogPrograms&);
	bool isSelfLoop(map<int, node>&, set<int>&, set<int>&, vector<int>&);
	void outRuleNumbers(set<int>&);
	//void findDuplicate(vector<set<int>>&, int);

private:
	database databaseProg;
	vector<relation> queriedDatabase;
	graph forwardGraph;
	graph backwardsGraph;
};

interpreter::interpreter()
{

}

void interpreter::addSchemes(datalogPrograms& program)
{
	for (unsigned int i = 0; i < program.getSchemes().size(); i++)
	{
		string tempName;
		scheme tempScheme;
		tempName = program.getSchemes()[i].getID().getValue();
		for (unsigned int y = 0; y < program.getSchemes()[i].getParam().size(); y++)
		{
			tempScheme.push_back(program.getSchemes()[i].getParam()[y].getValue());
		}
		databaseProg.addRelation(tempName, tempScheme);
	}
}

void interpreter::addTuples(datalogPrograms& program)
{
	for (unsigned int i = 0; i < program.getFacts().size(); i++)
	{
		string tempName;
		tuples tempTuple;
		tempName = program.getFacts()[i].getID().getValue();
		for (unsigned int y = 0; y < program.getFacts()[i].getParam().size(); y++)
		{
			tempTuple.push_back(program.getFacts()[i].getParam()[y].getValue());
		}
		databaseProg.addTuple(tempName, tempTuple);
	}
}

void interpreter::doQuery(datalogPrograms& program)
{
	for (unsigned int i = 0; i < program.getQueries().size(); i++)
	{
		//Query Storage
		string tempName = program.getQueries()[i].getID().getValue();
		tuples tempTuple;
		vector<bool> tempTupleID;
		vector<int>	tempTupleSize;
		//Other
		relation tempRel = databaseProg.getRelation(tempName);
		scheme tempScheme;
		for (unsigned int y = 0; y < program.getQueries()[i].getParam().size(); y++)
		{
			bool alreadySeen = false;
			for (unsigned int q = 0; q < tempTuple.size(); q++)
			{
				if (program.getQueries()[i].getParam()[y].getValue() == tempTuple[q])
				{
					alreadySeen = true;
					break;
				}
			}
			tempTuple.push_back(program.getQueries()[i].getParam()[y].getValue());
			if (program.getQueries()[i].getParam()[y].getType() == "ID")
			{
				tempTupleID.push_back(true);
				if (alreadySeen == false)
					tempTupleSize.push_back(y);
				else;
			}
			else
				tempTupleID.push_back(false);
		}
		selectQuery(tempTuple, tempTupleID, tempScheme, tempRel);
		tempRel = tempRel.project(tempTupleSize);
		tempRel = tempRel.rename(tempScheme);
		queriedDatabase.push_back(tempRel);
	}
}

void interpreter::printQuery(datalogPrograms& program)
{
	cout << "Query Evaluation\n";
	vector<parameters> parameters;
	vector<predicates> queries = program.getQueries();
	for (unsigned int i = 0; i < queries.size(); i++)
	{
		cout << queries[i].getID().getValue() << "(";
		parameters = queries[i].getParam();
		for (unsigned int y = 0; y < parameters.size(); y++)
		{
			if (y != parameters.size() - 1)
				cout << parameters[y].getValue() << ",";
			else
				cout << parameters[y].getValue();
		}
		if (queriedDatabase[i].getTuple().size() > 0)
		{
			cout << ")? Yes(" << queriedDatabase[i].getTuple().size() << ")\n";
		}
		else
			cout << ")? No\n";
		printResult(i);
		
	}
}

void interpreter::selectQuery(tuples& tempTuple, vector<bool>& tempTupleID, scheme& tempScheme, relation& tempRel)
{
	for (unsigned int x = 0; x < tempTuple.size(); x++)
	{
		if (tempTupleID[x] == true)
		{
			bool exists = false;
			for (unsigned int q = 0; q < tempScheme.size(); q++)
			{
				if (tempTuple[x] == tempScheme[q])
					exists = true;
			}
			if (exists == false)
				tempScheme.push_back(tempTuple[x]); //check here
			else
			{
				for (unsigned int i = 0; i < tempTuple.size(); i++)
				{
					if (tempTuple[i] == tempTuple[x])
					{
						tempRel = tempRel.selectID(i, x);
						break;
					}
				}
			}
		}
		else
			tempRel = tempRel.select(x, tempTuple[x]);
	}
}

void interpreter::printResult(unsigned int& i)
{
	set<tuples> temp = queriedDatabase[i].getTuple();
	for (auto it = temp.begin(); it != temp.end(); it++)
	{
		for (unsigned int r = 0; r < queriedDatabase[i].getScheme().size(); r++)
		{
			if (r == 0)
				cout << "  ";
			cout << queriedDatabase[i].getScheme()[r] << "=" << (*it)[r];
			if (r != queriedDatabase[i].getScheme().size() - 1)
				cout << ", ";
			else
				cout << endl;
		}
	}
}

void interpreter::evaluateRule(datalogPrograms& program, int rule)
{
	vector<relation> toJoin;
	doRule(program, rule, toJoin);

	relation joinedRelation = toJoin[0];
	for (unsigned int f = 1; f < toJoin.size(); f++)
	{
		scheme temp = toJoin[f].getScheme();
		set<tuples> temp2 = toJoin[f].getTuple();
		joinedRelation = joinedRelation.join(temp, temp2);
	}
	joinedRelation.setName(program.getRules()[rule].getHeadPred().getValue());

	projectAndRenameAfterJoin(program, joinedRelation, rule);

	set<tuples> temp = joinedRelation.getTuple();
	for (auto i = temp.begin(); i != temp.end(); i++)
	{
		databaseProg.addTuple(joinedRelation.getName(), *i);
	}
}

void interpreter::fixedPoint(datalogPrograms& program, int& numPasses, vector<int> whichRules)
{
	/*int size = databaseProg.getSize();
	for (unsigned int i = 0; i < program.getRules().size(); i++)
		evaluateRule(program, i);
	numPasses++;
	if (size == databaseProg.getSize())
		return;
	else
		fixedPoint(program, numPasses);*/

	int size = databaseProg.getSize();
	for (unsigned int i = 0 ; i  < whichRules.size(); i++)
		evaluateRule(program, whichRules[i]);
	numPasses++;
	if (size == databaseProg.getSize())
		return;
	else
		fixedPoint(program, numPasses, whichRules);
}

void interpreter::projectAndRenameAfterJoin(datalogPrograms& program, relation& joinedRelation, int rule)
{
	vector<int>	tempTupleSize;
	scheme tempScheme;
	for (unsigned int i = 0; i < program.getRules()[rule].getHeadPredIDs().size(); i++)
	{
		scheme tempScheme2 = joinedRelation.getScheme();
		for (unsigned int z = 0; z < tempScheme2.size(); z++)
		{
			if (tempScheme2[z] == program.getRules()[rule].getHeadPredIDs()[i].getValue())
				tempTupleSize.push_back(z);
		}
	}
	joinedRelation = joinedRelation.project(tempTupleSize);
	vector<predicates> tempPred = program.getSchemes();
	for (unsigned int i = 0; i < tempPred.size(); i++)
	{
		if (tempPred[i].getID().getValue() == program.getRules()[rule].getHeadPred().getValue())
		{
			for (unsigned int y = 0; y < tempPred[i].getParam().size(); y++)
				tempScheme.push_back(tempPred[i].getParam()[y].getValue());
		}
	}
	joinedRelation = joinedRelation.rename(tempScheme);
}

void interpreter::doRule(datalogPrograms& program, int rule, vector<relation>& toJoin)
{
	for (unsigned int i = 0; i < program.getRules()[rule].getIDs().size(); i++)
	{
		string tempName = program.getRules()[rule].getIDs()[i].getID().getValue();
		tuples tempTuple;
		vector<bool> tempTupleID;
		vector<int>	tempTupleSize;
		//if it doesn't show up?
		relation tempRel = databaseProg.getRelation(tempName);
		scheme tempScheme;
		for (unsigned int j = 0; j < program.getRules()[rule].getIDs()[i].getParam().size(); j++)
		{
			bool alreadySeen = false;
			for (unsigned int q = 0; q < tempTuple.size(); q++)
			{
				if (program.getRules()[rule].getIDs()[i].getParam()[j].getValue() == tempTuple[q])
				{
					alreadySeen = true;
					break;
				}
			}
			tempTuple.push_back(program.getRules()[rule].getIDs()[i].getParam()[j].getValue());
			if (program.getRules()[rule].getIDs()[i].getParam()[j].getType() == "ID")
			{
				tempTupleID.push_back(true);
				if (alreadySeen == false)
					tempTupleSize.push_back(j);
				else;
			}
			else
				tempTupleID.push_back(false);
		}
		selectQuery(tempTuple, tempTupleID, tempScheme, tempRel);
		tempRel = tempRel.project(tempTupleSize);
		tempRel = tempRel.rename(tempScheme);
		toJoin.push_back(tempRel);
	}
}

void interpreter::createGraph(datalogPrograms& program)
{
	//for each rule (rule 1)
	for (unsigned int i = 0; i < program.getRules().size(); i++)
	{
		bool added = false;
		//for each predicate name n in rule 1
		for (unsigned int j = 0; j < program.getRules()[i].getIDs().size(); j++)
		{
			//for each rule (rule 2)
			for (unsigned int k = 0; k < program.getRules().size(); k++)
			{
				//if n == rule 2's head name

				if (program.getRules()[i].getIDs()[j].getID().getValue() == program.getRules()[k].getHeadPred().getValue())
				{
					forwardGraph.addEdge(i, k);
					backwardsGraph.addEdge(k, i);
					added = true;
				}
			}
		}
		if (added == false)
		{
			forwardGraph.insertIntoGraph(i);
			backwardsGraph.insertIntoGraph(i);
		}
	}
	backwardsGraph.dfsForest();
	forwardGraph.dfsForestFromReverse(backwardsGraph);
	//forwardGraph.isCyclic();
}

void interpreter::outDependencyGraph()
{
	cout << "Dependency Graph\n";
	map<int, node> tempMap = forwardGraph.getMap();
	for (auto a = tempMap.begin(); a != tempMap.end(); a++)
	{
		//int z = a->first;
		cout << "R" << a->first << ":";
		set<int> tempSet = a->second.getNodeSet();
		unsigned int num = 0;
		for (auto b = tempSet.begin(); b != tempSet.end(); b++)
		{
			if (num == tempSet.size() - 1)
				cout << "R" << *b;// << endl;
			else
				cout << "R" << *b << ",";
			num++;
		}
		cout << endl;
	}
	cout << endl;
}

void interpreter::toFixedPoint(datalogPrograms& program)
{
	cout << "Rule Evaluation\n";
	int numPasses = 0;
	vector<set<int>> tempSCC = forwardGraph.getSCCVec();
	map<int, node> tempMap = forwardGraph.getMap();
	set<int> tempSet2;
	for (int i = 0; i < forwardGraph.getSCCSize(); i++)
	{
		vector<int> whichRules;
		set<int> tempSet = tempSCC[i];

		for (auto j = tempSet.begin(); j != tempSet.end(); j++)
		{
			int temp = *j;
			whichRules.push_back(temp);
		}
		

		/*for (unsigned int k = 0; k < tempSCC.size(); k++)
		{
			if (i != k && tempSCC[i] == tempSCC[k])
			{
				set<int> emptySet;
				tempSCC[k] = emptySet;
			}
		}*/
		// For bypassing duplicate sets
		//findDuplicate(tempSCC, i);

		if (tempSet.size() == 0)
			continue;
		else if (isSelfLoop(tempMap, tempSet, tempSet2, whichRules) == false && tempSet.size() == 1)
		{
			fixedPoint(program, numPasses, whichRules);
			cout << 1 << " passes: ";
		}
		else
		{
			fixedPoint(program, numPasses, whichRules);
			cout << numPasses << " passes: ";
		}
		outRuleNumbers(tempSet);
		numPasses = 0;
		cout << endl;
	}
	cout << endl;
}

bool interpreter::isSelfLoop(map<int,node>& tempMap, set<int>& tempSet, set<int>& tempSet2, vector<int>& whichRules)
{
	bool isSelfLoop = false;
	for (auto a = tempMap.begin(); a != tempMap.end(); a++)
	{
		if (whichRules.size() != 0 && a->first == whichRules[0])
			tempSet2 = a->second.getNodeSet();
	}
	for (auto b = tempSet2.begin(); b != tempSet2.end(); b++)
	{
		//Can't contain itself
		if (tempSet.size() != 0 && *b == *tempSet.begin())
			isSelfLoop = true;
	}
	return isSelfLoop;
}

void interpreter::outRuleNumbers(set<int>& tempSet)
{
	for (auto l = tempSet.begin(); l != tempSet.end();)
	{
		cout << "R" << *l;
		//auto temp = l;
		l++;
		if (l == tempSet.end())
			break;
		else
			cout << ",";
	}
}

/*
void interpreter::findDuplicate(vector<set<int>>& tempSCC, int i)
{
	for (int k = 0; k < (int)tempSCC.size(); k++)
	{
		if (i != k && tempSCC[i] == tempSCC[k])
		{
			set<int> emptySet;
			tempSCC[k] = emptySet;
		}
		//bool isDup = false;
		if (i != k)
		{
			set<int> tempSet1 = tempSCC[i];
			set<int> tempSet2 = tempSCC[k];
			for (auto q = tempSet1.begin(); q != tempSet1.end(); q++)
			{
				for (auto j = tempSet2.begin(); j !=tempSet2.end(); j++)
				{
					if (*q == *j)
					{
						set<int> emptySet;
						if (tempSet1.size() > tempSet2.size())
							tempSCC[k] = emptySet;
						else
							tempSCC[i] = emptySet;
					}
				}
			}
		}
	}
}*/