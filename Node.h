#pragma once
#include <set>
using namespace std;

class node
{
public:
	void markVisited(bool x)
	{
		visited = x;
	}
	set<int> getNodeSet()
	{
		return nodeSet;
	}
	bool alreadyVisited()
	{
		return visited;
	}
	void setPostOrderNumber(int x)
	{
		postOrder = x;
	}
	void addToNodeSet(int x)
	{
		nodeSet.insert(x);
	}
	int getPostOrderNumber()
	{
		return postOrder;
	}

private:
	set<int> nodeSet;
	bool visited = false;
	int postOrder;
};