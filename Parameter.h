#pragma once
#include "Expressions.h"
using namespace std;

class parameters
{
public:
	parameters(Token input);
	parameters();

	string getValue()
	{
		if (type != "expression")
			return value;
		else
			return expression.getExp();
	}

	string getType()
	{
		return type;
	}

private:
	string type;
	string value;
	expressions expression;
};

parameters::parameters(Token input)
{
	if (input.getType() == "ID")
	{
		type = "ID";
		value = input.getValue();
	}
	else if (input.getType() == "STRING")
	{
		type = "STRING";
		value = input.getValue();
	}
	else
	{
		type = "expression";
		expression.setExp(input.getValue());
	}

}

parameters::parameters()
{

}