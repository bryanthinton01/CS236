#include "Scanner.h"

using namespace std;

Scanner::Scanner()
{
}
void Scanner::scanTok(vector<Token> &toks, ifstream& in, int& lineNum, int &c, Token tok)
{
	string tempVal = "";
	string tempString = "";
	int count = 1;
	while (isspace(c))
	{
		if (c == '\n')
			lineNum++;
		c = in.get();
	}
	switch (c)
	{
	case '\n':
		lineNum++;
		break;
	case ',':
		tok.setType(Token::COMMA);
		tok.setValue(",");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case '.':
		tok.setType(Token::PERIOD);
		tok.setValue(".");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case '?':
		tok.setType(Token::Q_MARK);
		tok.setValue("?");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case '(':
		tok.setType(Token::LEFT_PAREN);
		tok.setValue("(");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case ')':
		tok.setType(Token::RIGHT_PAREN);
		tok.setValue(")");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case '*':
		tok.setType(Token::MULTIPLY);
		tok.setValue("*");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case '+':
		tok.setType(Token::ADD);
		tok.setValue("+");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	case ':':
		colonCase(toks, in, lineNum, c, tok);
		break;
	case '\'':
		tok.setLineNum(lineNum);
		tok.setValue(scanString(toks, in, lineNum, c, tok, count));
		if (count == 0)
			tok.setType(Token::STRING);
		else
			tok.setType(Token::UNDEFINED);
		toks.push_back(tok);
		scanTok(toks, in, lineNum, c, tok);
		break;
	case '#':
		commentCase(toks, in, lineNum, c, tok);
		break;
	case 'S':
		sCase(toks, in, lineNum, c, tok);
		break;
	case 'F':
		fCase(toks, in, lineNum, c, tok);
		break;
	case 'R':
		rCase(toks, in, lineNum, c, tok);
		break;
	case 'Q':
		qCase(toks, in, lineNum, c, tok);
		break;
	case -1:
		tok.setType(Token::ENDOFFILE);
		tok.setValue("");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		break;
	default:
		defaultCase(toks, in, lineNum, c, tok);
		break;
	}
}
string Scanner::scanString(vector<Token>& toks, ifstream& in, int &lineNum, int &c, Token tok, int& count)
{
	string word;
	count = 1;
	bool done;
	word = (char)c;
	while (c == '\'')
	{
		done = stringExt(c, count, word, lineNum, in);
		if (done == true)
			break;
	}
	return word;
}
string Scanner::scanComment(vector<Token>& toks, ifstream& in, int &lineNum, int &c, Token tok)
{
	string word = "#";
	if (c == '\n')
	{
		lineNum++;
		c = in.get();
		return word;
	}
	word += (char)c;
	c = in.get();
	while (c != '\n' && c != -1)
	{
		word += (char)c;
		c = in.get();
	}
	return word;
}
string Scanner::scanMLComment(vector<Token>& toks, ifstream& in, int &lineNum, int &c, Token tok, string& tempString)
{
	string word = tempString;
	word += (char)c;
	do
	{
		c = in.get();
		while (c != '|' && c != -1)
		{
			word += (char)c;
			c = in.get();
			if (c == -1)
			{
				return word;
			}
			lineInc(lineNum, c);
		}
		word += (char)c;
		c = in.get();
		word += (char)c;
	} while (c != '#' && c != -1);
	return word;
}
string Scanner::scanStringNQ(vector<Token>& toks, ifstream& in, int &lineNum, int &c, Token tok)
{
	string word;
	word = (char)c;
	c = in.get();
	while (isalnum(c))
	{
		word += (char)c;
		c = in.get();
	}
	lineInc(lineNum, c);
	return word;
}
void Scanner::lineInc(int &lineNum, int& c)
{
	if (c == '\n')
		lineNum++;
	else
		return;
}
void Scanner::colonCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	c = in.get();
	if (c == '-')
	{
		tok.setType(Token::COLON_DASH);
		tok.setValue(":-");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		return;
	}
	else
	{
		tok.setType(Token::COLON);
		tok.setValue(":");
		tok.setLineNum(lineNum);
		toks.push_back(tok);
		scanTok(toks, in, lineNum, c, tok);
		return;
	}
}
void Scanner::commentCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	int tempLN = 0;
	string tempString = "";
	tempString = c;
	tempLN = lineNum;
	c = in.get();
	if (c == '|')
	{
		tok.setValue(scanMLComment(toks, in, lineNum, c, tok, tempString));
		if (c == -1 && tok.getValue().back() != '#')
			tok.setType(Token::UNDEFINED);
		else
			tok.setType(Token::COMMENT);
		tok.setLineNum(tempLN);
		toks.push_back(tok);
		if (c == -1)
			scanTok(toks, in, lineNum, c, tok);
		return;
	}
	else
	{
		tok.setValue(scanComment(toks, in, lineNum, c, tok));
		tok.setType(Token::COMMENT);
		tok.setLineNum(tempLN);
		toks.push_back(tok);
		scanTok(toks, in, lineNum, c, tok);
		return;
	}
}
void Scanner::defaultCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	string word;
	tok.setLineNum(lineNum);
	if (isalpha(c))
	{
		while (isalnum(c))
		{
			word += (char)c;
			c = in.get();
			lineInc(lineNum, c);
		}
		tok.setValue(word);
		tok.setType(Token::ID);
	}
	else
	{
		word = (char)c;
		tok.setValue(word);
		tok.setType(Token::UNDEFINED);
		c = in.get();
		lineInc(lineNum, c);
	}
	toks.push_back(tok);
	if (c != '\n')
		scanTok(toks, in, lineNum, c, tok);
	return;
}
void Scanner::qCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	string tempVal = "";
	tok.setLineNum(lineNum);
	tempVal = scanStringNQ(toks, in, lineNum, c, tok);
	if (tempVal == "Queries")
	{
		tok.setValue(tempVal);
		tok.setType(Token::QUERIES);
	}
	else
	{
		tok.setValue(tempVal);
		tok.setType(Token::ID);
	}
	toks.push_back(tok);
	if (isspace(c) == false)
		scanTok(toks, in, lineNum, c, tok);
	return;
}
void Scanner::fCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	string tempVal = "";
	tok.setLineNum(lineNum);
	tempVal = scanStringNQ(toks, in, lineNum, c, tok);
	if (tempVal == "Facts")
	{
		tok.setValue(tempVal);
		tok.setType(Token::FACTS);
	}
	else
	{
		tok.setValue(tempVal);
		tok.setType(Token::ID);
	}
	toks.push_back(tok);
	if (isspace(c) == false)
		scanTok(toks, in, lineNum, c, tok);
	return;
}
void Scanner::sCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	string tempVal = "";
	tok.setLineNum(lineNum);
	tempVal = scanStringNQ(toks, in, lineNum, c, tok);
	if (tempVal == "Schemes")
	{
		tok.setValue(tempVal);
		tok.setType(Token::SCHEMES);
	}
	else
	{
		tok.setValue(tempVal);
		tok.setType(Token::ID);
	}
	toks.push_back(tok);
	if (isspace(c) == false)
		scanTok(toks, in, lineNum, c, tok);
	return;
}
void Scanner::rCase(vector<Token>& toks, ifstream& in, int& lineNum, int&c, Token tok)
{
	string tempVal = "";
	tok.setLineNum(lineNum);
	tempVal = scanStringNQ(toks, in, lineNum, c, tok);
	if (tempVal == "Rules")
	{
		tok.setValue(tempVal);
		tok.setType(Token::RULES);
	}
	else
	{
		tok.setValue(tempVal);
		tok.setType(Token::ID);
	}
	toks.push_back(tok);
	if (isspace(c) == false)
		scanTok(toks, in, lineNum, c, tok);
	return;
}
bool Scanner::stringExt(int&c, int& count, string& word, int& lineNum, ifstream& in)
{
	do
	{
		c = in.get();
		if ((word.back() == '\'' && count % 2 == 0 && c != '\'') || c == -1)
			break;
		lineInc(lineNum, c);
		word += (char)c;
	} while (c != '\'' && c != -1);
	if (c == '\'')
	{
		count++;
		return false;
	}
	else
	{
		count = count % 2;
		return true;
	}
}