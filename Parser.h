#pragma once
#include "Token.h"
#include "Scanner.h"
#include "DatalogProgram.h"
#include <set>

using namespace std;

class Parser
{
public:
	datalogPrograms Parse(vector<Token> toks);
	void match(string t);
	void getToken();
	void Error();
	void datalogProgram();
	predicates scheme();
	void schemeList();
	void idList(predicates& temp);
	predicates fact();
	void factList();
	rules rule();
	void ruleList();
	void headPredicate(rules& temp);
	void predicate(predicates& temp);
	void predicateList(predicates& temp);
	void parameter();
	void parameterList(predicates& temp);
	string expression();
	void operation();
	predicates query();
	void queryList();
	void stringList(predicates& temp);
	void idListRule(rules& temp);
	void predicateRule(rules& temp, int& index);
	void predicateListRule(rules& temp, int& index);
	void parameterListRule(rules& temp, int& index);
	void parameterRule(rules& temp, int& index);
	void rulesToString(datalogPrograms& prog);
	void queriesToString(datalogPrograms& prog);
	set<string> factsToString(datalogPrograms& prog);
	void schemesToString(datalogPrograms& prog);
	void domainToString(set<string>& domain);
	
private:
	Token token;
	vector<Token> toks;
	int currentToken = 0;
	datalogPrograms prog;

};
datalogPrograms Parser::Parse(vector<Token> toks)
{
	for (unsigned int i = 0; i < toks.size(); i++)
	{
		if (toks[i].getType() != "COMMENT")
			(this->toks).push_back(toks[i]);
	}
	getToken();
	try
	{
		datalogProgram();
	}
	catch (Token token)
	{
		//cout << "Failure!\n\t" << "(" << token.getType() << "," << "\"" << token.getValue() << "\"" << "," << token.getLineNum() << ")" << endl;
		return prog;
	}
	//if (token.getType() != "EOF");
		//cout << "Failure!\n\t" << "(" << token.getType() << "," << "\"" << token.getValue() << "\"" << "," << token.getLineNum() << ")" << endl;
	//else
	//{
		//set<string> domain;
		//cout << "Success!" << endl;
		//schemesToString(prog);
		//domain = factsToString(prog);
		//rulesToString(prog);
		//queriesToString(prog);
		//domainToString(domain);
	//}
	return prog;
}
void Parser::getToken()
{
	token = toks[currentToken];
	currentToken++;
}
void Parser::match(string t)
{
	if (token.getType() == t)
	{
		getToken();
	}
	else
		Error();
}
void Parser::Error()
{
	throw token;
}
void Parser::datalogProgram()
{
	if (token.getType() == "SCHEMES")
	{
		match("SCHEMES"); match("COLON");
		prog.addSchemes(scheme());
		schemeList();
		match("FACTS"); match("COLON"); factList();
		match("RULES"); match("COLON"); ruleList();
		match("QUERIES"); match("COLON"); 
		prog.addQueries(query());
		queryList();
	}
	else
		Error();
}
predicates Parser::scheme()
{
	predicates temp(token);
	if (token.getType() == "ID")
	{
		match("ID"); match("LEFT_PAREN");
		if (token.getType() == "ID")
		{
			temp.addParam(token);
		}
		match("ID"); idList(temp); match("RIGHT_PAREN");
		return temp;
	}
	else
		Error();
	return temp;
}
void Parser::schemeList()
{
	if (token.getType() == "ID")
	{
		prog.addSchemes(scheme()); schemeList();
	}
	else;
}
void Parser::idList(predicates &temp)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA");
		if (token.getType() == "ID")
		{
			temp.addParam(token);
		}
		match("ID"); idList(temp);
	}
	else;
}
predicates Parser::fact()
{
	predicates temp(token);
	if (token.getType() == "ID")
	{
		match("ID"); match("LEFT_PAREN");
		if (token.getType() == "STRING")
		{
			temp.addParam(token);
		}
		match("STRING"); stringList(temp); match("RIGHT_PAREN"); match("PERIOD");
		return temp;
	}
	else
		Error();
	return temp;
}
void Parser::factList()
{
	if (token.getType() == "ID")
	{
		prog.addFacts(fact()); factList();
	}
	else;
}
rules Parser::rule()
{
	int index = 0;
	rules temp(token);
	if (token.getType() == "ID")
	{
		headPredicate(temp); match("COLON_DASH"); predicateRule(temp, index); predicateListRule(temp, index); match("PERIOD");
	}
	else
		Error();
	return temp;
}
void Parser::ruleList()
{
	if (token.getType() == "ID")
	{
		prog.addRules(rule()); ruleList();
	}
	else;
}
void Parser::headPredicate(rules& temp)
{
	if (token.getType() == "ID")
	{
		match("ID"); match("LEFT_PAREN");
		if (token.getType() == "ID")
		{
			temp.addHeadPredID(token);
		}
		match("ID"); idListRule(temp); match("RIGHT_PAREN");
	}
	else
		Error();
}
void Parser::predicate(predicates& temp)
{
	if (token.getType() == "ID")
	{
		match("ID"); match("LEFT_PAREN"); 
		if (token.getType() == "ID" || token.getType() == "STRING")
		{
			temp.addParam(token);
		}
		else if (token.getType() == "LEFT_PAREN")
		{
			Token tempTok;
			tempTok.setValue(expression());
			temp.addParam(tempTok);
		}
		parameter(); parameterList(temp); match("RIGHT_PAREN");
	}
	else
		Error();
}
void Parser::predicateList(predicates& temp)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA"); predicate(temp); predicateList(temp);
	}
	else;
}
void Parser::parameter()
{
	string temp;
	if (token.getType() == "STRING")
		match("STRING");
	else if (token.getType() == "ID")
		match("ID");
	else if (token.getType() == "LEFT_PAREN")
		temp = expression();
	else
		Error();
}
void Parser::parameterList(predicates& temp)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA"); 
		if (token.getType() == "ID" || token.getType() == "STRING")
		{
			temp.addParam(token);
		}
		else if (token.getType() == "LEFT_PAREN")
		{
			Token tempTok;
			tempTok.setValue(expression());
			temp.addParam(tempTok);
		}
		parameter(); parameterList(temp);
	}
	else;
}
string Parser::expression()
{
	string temp;
	if (token.getType() == "LEFT_PAREN")
	{
		temp += token.getValue(); match("LEFT_PAREN");
		temp += token.getValue(); parameter(); 
		temp += token.getValue(); operation(); 
		temp += token.getValue(); parameter(); 
		temp += token.getValue(); match("RIGHT_PAREN");
	}
	else
		Error();
	return temp;
}
void Parser::operation()
{
	if (token.getType() == "MULTIPLY")
		match("MULTIPLY");
	else if (token.getType() == "ADD")
		match("ADD");
	else
		Error();
}
predicates Parser::query()
{
	predicates temp(token);
	if (token.getType() == "ID")
	{
		predicate(temp); match("Q_MARK");
	}
	else
		Error();
	return temp;
}
void Parser::queryList()
{
	if (token.getType() == "ID")
	{
		prog.addQueries(query()); queryList();
	}
	else;
}
void Parser::stringList(predicates& temp)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA"); 
		if (token.getType() == "STRING")
		{
			temp.addParam(token);
		}
		match("STRING"); stringList(temp);
	}
	else;
}
void Parser::idListRule(rules &temp)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA");
		if (token.getType() == "ID")
		{
			temp.addHeadPredID(token);
		}
		match("ID"); idListRule(temp);
	}
	else;
}
void Parser::predicateRule(rules& temp, int& index)
{
	if (token.getType() == "ID")
	{
		temp.addID(token);
		match("ID"); match("LEFT_PAREN");
		parameterRule(temp, index); 
		parameterListRule(temp, index); match("RIGHT_PAREN"); index++;
	}
	else
		Error();
}
void Parser::predicateListRule(rules& temp, int& index)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA"); predicateRule(temp, index); predicateListRule(temp, index);
	}
	else;
}
void Parser::parameterListRule(rules& temp, int& index)
{
	if (token.getType() == "COMMA")
	{
		match("COMMA");
		parameterRule(temp, index); 
		parameterListRule(temp, index);
	}
	else;
}
void Parser::parameterRule(rules& temp, int& index)
{
	if (token.getType() == "STRING")
	{
		temp.addParam(index, token);
		match("STRING");
	}
	else if (token.getType() == "ID")
	{
		temp.addParam(index, token);
		match("ID");
	}
	else if (token.getType() == "LEFT_PAREN")
	{
		Token tempTok;
		tempTok.setValue(expression());
		temp.addParam(index, tempTok);
	}
	else
		Error();
}
void Parser::rulesToString(datalogPrograms& prog)
{
	vector<parameters> parameters;
	vector<rules> rules = prog.getRules();
	vector<predicates> preds;
	cout << "Rules(" << rules.size() << "):\n";
	for (unsigned int i = 0; i < rules.size(); i++)
	{
		cout << "  " << rules[i].getHeadPred().getValue() << "(";
		parameters = rules[i].getHeadPredIDs();
		for (unsigned int y = 0; y < parameters.size(); y++)
		{
			if (y != parameters.size() - 1)
				cout << parameters[y].getValue() << ",";
			else
				cout << parameters[y].getValue();
		}
		cout << ") :- ";
		preds = rules[i].getIDs();
		for (unsigned int y = 0; y < preds.size(); y++)
		{
			if (y != 0)
				cout << ",";
			cout << preds[y].getID().getValue() << "(";
			parameters = preds[y].getParam();
			for (unsigned int q = 0; q < parameters.size(); q++)
			{
				if (q != parameters.size() - 1)
					cout << parameters[q].getValue() << ",";
				else
					cout << parameters[q].getValue() << ")";
			}
		}
		cout << ".\n";
	}
}
void Parser::queriesToString(datalogPrograms& prog)
{
	vector<parameters> parameters;
	vector<predicates> queries = prog.getQueries();
	cout << "Queries(" << queries.size() << "):\n";
	for (unsigned int i = 0; i < queries.size(); i++)
	{
		cout << "  " << queries[i].getID().getValue() << "(";
		parameters = queries[i].getParam();
		for (unsigned int y = 0; y < parameters.size(); y++)
		{
			if (y != parameters.size() - 1)
				cout << parameters[y].getValue() << ",";
			else
				cout << parameters[y].getValue();
		}
		cout << ")?\n";
	}
}
set<string> Parser::factsToString(datalogPrograms& prog)
{
	vector<parameters> parameters;
	vector<predicates> facts = prog.getFacts();
	set<string> domain;
	cout << "Facts(" << facts.size() << "):\n";
	for (unsigned int i = 0; i < facts.size(); i++)
	{
		cout << "  " << facts[i].getID().getValue() << "(";
		parameters = facts[i].getParam();
		for (unsigned int y = 0; y < parameters.size(); y++)
		{
			domain.insert(parameters[y].getValue());
			if (y != parameters.size() - 1)
				cout << parameters[y].getValue() << ",";
			else
				cout << parameters[y].getValue();
		}
		cout << ").\n";
	}
	return domain;
}
void Parser::schemesToString(datalogPrograms& prog)
{
	vector<parameters> parameters;
	vector<predicates> schemes = prog.getSchemes();
	cout << "Schemes(" << schemes.size() << "):\n";
	for (unsigned int i = 0; i < schemes.size(); i++)
	{
		cout << "  " << schemes[i].getID().getValue() << "(";
		parameters = schemes[i].getParam();
		for (unsigned int y = 0; y < parameters.size(); y++)
		{
			if (y != parameters.size() - 1)
				cout << parameters[y].getValue() << ",";
			else
				cout << parameters[y].getValue();
		}
		cout << ")\n";
	}
}
void Parser::domainToString(set<string>& domain)
{
	cout << "Domain(" << domain.size() << "):\n";
	for (auto it = domain.begin(); it != domain.end(); it++)
	{
		cout << "  " << *it << endl;
	}
}